package lab4_hibernate;

import lab4_hibernate.db_model.CardNumber;
import lab4_hibernate.db_model.MagazynpEntity;
import lab4_hibernate.db_model.SlownikEntity;
import lab4_hibernate.util.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;

import java.sql.Date;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        select();
    }

    public static void insert(){
        Session session = null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            SlownikEntity slownik = new SlownikEntity();
            slownik.setGr(9999);
            slownik.setPodGr(9999);
            slownik.setRodz(99999);
            slownik.setTyp("TT");
            slownik.setOpis("TT");
            session.save(slownik);

            MagazynpEntity magazyn = new MagazynpEntity();
            //magazyn.setNrMag(1); // autoincrement
            magazyn.setNrKarty(new CardNumber("00/00/0000"));
            magazyn.setSlownikByNrOdpadu(slownik);
            magazyn.setNrKlienta(99999);
            magazyn.setFirma(99999);
            magazyn.setJednostka("kila");
            magazyn.setMasa(99.9);
            magazyn.setDataD(new Date(1466297462));

            session.save(magazyn);

            session.getTransaction().commit();

            System.out.println("Done");
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            session.close();
        }
    }

    public static void select(){
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();

            session.beginTransaction();
            Query query = session.createQuery("from MagazynpEntity")
                    .setMaxResults(10)
                    ;

            List<MagazynpEntity> magazunp = query.list();

            for(MagazynpEntity item : magazunp) {
                System.out.println("nrMag: \t" + item.getNrMag());
                System.out.println("nrKarty: \t" + item.getNrKarty());
                System.out.println("slownikByNrOdpadu: \t" + item.getSlownikByNrOdpadu());
                System.out.println("nrKlienta: \t" + item.getNrKlienta());
                System.out.println("firma: \t" + item.getFirma());
                System.out.println("jednostka: \t" + item.getJednostka());
                System.out.println("masa: \t" + item.getMasa());
                System.out.println("dataD: \t" + item.getDataD());
                System.out.println("++++++++++++++++++++++++++++++++++++++++");
            }
            session.getTransaction().commit();


        }catch(Exception ex){
            ex.printStackTrace();
        }finally {
            session.close();
        }
    }

    public static void delete(){
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();

            session.beginTransaction();

            MagazynpEntity magazynp = (MagazynpEntity) session.load(MagazynpEntity.class, 2);
            session.delete(magazynp);

            session.getTransaction().commit();

        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            session.close();
        }
    }
}
