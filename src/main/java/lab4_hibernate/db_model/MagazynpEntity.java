package lab4_hibernate.db_model;

import lombok.*;

import java.io.Serializable;
import java.sql.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Order({"nrMag", "nrKarty", "slownikByNrOdpadu", "nrKlienta", "firma", "jednostka", "masa", "dataD"})
public class MagazynpEntity implements Serializable {

    private Integer nrMag;
    private CardNumber nrKarty;
    private SlownikEntity slownikByNrOdpadu;
    private Integer nrKlienta;
    private Integer firma;
    private String jednostka;
    private Double masa;
    private Date dataD;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MagazynpEntity that = (MagazynpEntity) o;

        if (nrMag != that.nrMag) return false;
        ///if (nrKarty != null ? !nrKarty.equals(that.nrKarty) : that.nrKarty != null) return false;
        if (nrKlienta != null ? !nrKlienta.equals(that.nrKlienta) : that.nrKlienta != null) return false;
        if (firma != null ? !firma.equals(that.firma) : that.firma != null) return false;
        if (jednostka != null ? !jednostka.equals(that.jednostka) : that.jednostka != null) return false;
        if (masa != null ? !masa.equals(that.masa) : that.masa != null) return false;
        if (dataD != null ? !dataD.equals(that.dataD) : that.dataD != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = nrMag;
        result = 31 * result + (nrKarty != null ? nrKarty.hashCode() : 0);
        result = 31 * result + (nrKlienta != null ? nrKlienta.hashCode() : 0);
        result = 31 * result + (firma != null ? firma.hashCode() : 0);
        result = 31 * result + (jednostka != null ? jednostka.hashCode() : 0);
        result = 31 * result + (masa != null ? masa.hashCode() : 0);
        result = 31 * result + (dataD != null ? dataD.hashCode() : 0);
        return result;
    }
}
