package lab4_hibernate.db_model;

import org.hibernate.HibernateException;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

public class CardNumberType implements UserType{
    /**
     * Returns the object from the 2 level cache
     */
    @Override
    public Object assemble(final Serializable cached, final Object owner)
            throws HibernateException {
        //would work as the AuditData.class is Serializable,
        //and stored in cache as it is - see disassemble
        return cached;
    }

    /**
     * Used to create Snapshots of the object
     */
    @Override
    public Object deepCopy(Object value) throws HibernateException {
        //return value; -> if AuditData.class was immutable we could return the object as it is
        final CardNumber recievedParam = (CardNumber) value;
        final CardNumber cardNumber = new CardNumber(recievedParam.toString());
        return cardNumber;
    }

    /**
     * method called when Hibernate puts the data in a second level cache. The data is stored
     * in a serializable form
     */
    @Override
    public Serializable disassemble(final Object value) throws HibernateException {
        return (Serializable) value;
    }

    /**
     * Used while dirty checking - control passed on to the {@link CardNumber}
     */
    @Override
    public boolean equals(final Object o1, final Object o2) throws HibernateException {
        boolean isEqual = false;
        if (o1 == o2) {
            isEqual = true;
        }
        if (null == o1 || null == o2) {
            isEqual = false;
        } else {
            isEqual = o1.equals(o2);
        }
        return isEqual;
        //for this to work correctly the equals()
        //method must be implemented correctly by CardNumber class
    }

    @Override
    public int hashCode(final Object value) throws HibernateException {
        return value.hashCode();
        //for this to work correctly the hashCode()
        //method must be implemented correctly by CardNumber class

    }

    /**
     * Helps hibernate apply certain optimizations for immutable objects
     */
    @Override
    public boolean isMutable() {
        return true; //The fields can be modified
    }

    /**
     * This method retrieves the property value from the JDBC resultSet
     */
    @Override
    public Object nullSafeGet(final ResultSet resultSet,
                              final String[] names, final Object owner)
            throws HibernateException, SQLException {
        //owner here is class from where the call to retrieve data was made.
        //In this case the Test class

        CardNumber cardNumber = null;
        final String nrKarty = resultSet.getString(names[0]);
        //Deferred check after first read
        if (!resultSet.wasNull()) {
            cardNumber = new CardNumber(nrKarty);
        }
        return cardNumber;
    }

    /**
     * The method writes the property value to the JDBC prepared Statement
     *
     */
    @Override
    public void nullSafeSet(final PreparedStatement statement,
                            final Object value, final int index) throws HibernateException,
            SQLException {
        if (null == value) {
            statement.setNull(index, StandardBasicTypes.STRING.sqlType());
        } else {
            CardNumber cardNumber = (CardNumber) value;
            System.out.println(("Saving object " + cardNumber + " for index " + index));
            statement.setString(index, cardNumber.toString());
        }
    }

    /**
     * Method used by Hibernate to handle merging of detached object.
     */
    @Override
    public Object replace(final Object original, final Object target,
                          final Object owner)
            throws HibernateException {
        //return original; // if immutable use this
        //For mutable types at bare minimum return a deep copy of first argument
        return this.deepCopy(original);

    }

    /**
     * Method tells Hibernate which Java class is mapped to this Hibernate Type
     */
    @SuppressWarnings("rawtypes")
    @Override
    public Class returnedClass() {
        return CardNumber.class;
    }

    /**
     * Method tells Hibernate what SQL columns to use for DDL schema generation.
     * using the Hibernate Types leaves Hibernate free to choose actual SQl types
     * based on database dialect.
     * (Alternatively SQL types can also be used directly)
     */
    @Override
    public int[] sqlTypes() {
        //createdBy, createdDate,modifiedBy,modifiedDate
        return new int[] { Types.VARCHAR };
    }
}
