package lab4_hibernate.db_model;

import lombok.*;

import java.io.Serializable;
import java.util.Collection;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(exclude="magazynpsByNrOdpadu")
public class SlownikEntity implements Serializable, Comparable<SlownikEntity>{

    private Integer gr;
    private Integer podGr;
    private Integer rodz;
    private String typ;
    private String opis;
    private Integer nrOdpadu;
    private Collection<MagazynpEntity> magazynpsByNrOdpadu;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SlownikEntity that = (SlownikEntity) o;

        if (nrOdpadu != that.nrOdpadu) return false;
        if (gr != null ? !gr.equals(that.gr) : that.gr != null) return false;
        if (podGr != null ? !podGr.equals(that.podGr) : that.podGr != null) return false;
        if (rodz != null ? !rodz.equals(that.rodz) : that.rodz != null) return false;
        if (typ != null ? !typ.equals(that.typ) : that.typ != null) return false;
        if (opis != null ? !opis.equals(that.opis) : that.opis != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = nrOdpadu;
        result = 31 * result + (gr != null ? gr.hashCode() : 0);
        result = 31 * result + (podGr != null ? podGr.hashCode() : 0);
        result = 31 * result + (rodz != null ? rodz.hashCode() : 0);
        result = 31 * result + (typ != null ? typ.hashCode() : 0);
        result = 31 * result + (opis != null ? opis.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(SlownikEntity o) {
        return nrOdpadu.compareTo(o.getNrOdpadu());
    }
}
