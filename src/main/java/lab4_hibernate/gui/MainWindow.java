package lab4_hibernate.gui;

import lab4_hibernate.db_model.MagazynpEntity;
import lab4_hibernate.db_model.SlownikEntity;
import lab4_hibernate.util.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.List;

public class MainWindow {
    private JPanel panel1;
    private JPanel panelTop;
    private JPanel panelBottom;
    private JPanel panelCenter;
    private JTable table1;

    private JPopupMenu popupMenu;

    public static Session session = HibernateUtil.getSessionFactory().openSession();

    public static void main(String[] args) {
        JFrame frame = new JFrame("MainWindow");
        frame.setContentPane(new MainWindow().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public MainWindow(){
        initTable();
        initPopupMenu();
    }

    private void initTable(){
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            Query query = session.createQuery("from MagazynpEntity");
            List<MagazynpEntity> listaMagazynp = query.list();

            session.getTransaction().commit();

            table1.setModel(new TableModel(listaMagazynp));
        }catch(Exception ex){
            ex.printStackTrace();
        }

        table1.getColumnModel().getColumn(2).setCellRenderer( new DefaultTableCellRenderer() {
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component cellComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                SlownikEntity entity = (SlownikEntity) value;
                setText(entity.getNrOdpadu() + ": " + entity.getOpis());
                return cellComponent;
            }
        });
    }

    private void initPopupMenu(){
        popupMenu = new JPopupMenu();

        JMenuItem im = new JMenuItem("Insert");
        im.setActionCommand("insert");
        im.addActionListener(event -> popupMenuListener(event));


        JMenuItem em = new JMenuItem("Edit");
        em.setActionCommand("edit");
        em.addActionListener(event -> popupMenuListener(event));


        JMenuItem dm = new JMenuItem("Delete");
        dm.setActionCommand("delete");
        dm.addActionListener(event -> popupMenuListener(event));


        popupMenu.add(im);
        popupMenu.add(em);
        popupMenu.add(dm);

        table1.setComponentPopupMenu(popupMenu);
    }

    private void popupMenuListener(ActionEvent event){
        InsertEditMagazynDialog dialog = null;
        TableModel m = (TableModel) table1.getModel();
        MagazynpEntity entity = null;

        switch (event.getActionCommand()){
            case "insert":
                dialog = new InsertEditMagazynDialog();
                dialog.pack();
                dialog.setVisible(true);

                if (dialog.getMagazynpEntity() != null){
                    entity = dialog.getMagazynpEntity();

                    session.beginTransaction();
                    session.save(entity);
                    session.getTransaction().commit();

                    m.addRows(entity);
                }
                break;

            case "edit":
                entity = (MagazynpEntity) m.getRowAt(table1.getSelectedRow());
                dialog = new InsertEditMagazynDialog(entity);
                dialog.pack();
                dialog.setVisible(true);

                if (dialog.getMagazynpEntity() != null){
                    entity = dialog.getMagazynpEntity();

                    session.beginTransaction();
                    session.update(entity);
                    session.getTransaction().commit();

                    m.updateRow(table1.getSelectedRow(), entity);
                    table1.repaint();
                }
                break;

            case "delete":
                entity = (MagazynpEntity) m.getRowAt(table1.getSelectedRow());
                try {

                    session.beginTransaction();
                    session.delete(entity);
                    session.getTransaction().commit();

                    m.removeRow(table1.getSelectedRow());
                    table1.repaint();

                }catch(Exception ex){
                    ex.printStackTrace();
                }
                break;

            default: break;
        }
    }
}
