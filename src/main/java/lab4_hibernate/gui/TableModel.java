package lab4_hibernate.gui;

import lab4_hibernate.db_model.Order;

import javax.swing.table.AbstractTableModel;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class TableModel <T> extends AbstractTableModel {
    private List<String> columnNames;
    public List<T> rows;
    private List<String> methods = new ArrayList<>();

    public TableModel(List<T> rows) throws Exception {
        this.rows = rows;

        Class<?> c = rows.get(0).getClass();
        List<Method> methods = Arrays.asList(c.getDeclaredMethods());

        this.methods = methods.stream()
                .map(Method::getName)
                .filter(t -> t.startsWith("get"))
                .collect(Collectors.toList());

        // jeśli jest adnotacja
        if (c.isAnnotationPresent(Order.class)) {
            Order orderAnnotation = c.getAnnotation(Order.class);
            List<String> order = Arrays.asList(orderAnnotation.value());
            List<String> fields = Arrays.asList(c.getDeclaredFields()).stream()
                    .map(Field::getName).collect(Collectors.toList());

            // wielkość się zgadza
            if ( fields.size() == order.size() ){
                //czy nazwy pól się zgadzaja
                for (String ord: order) {
                    if (!fields.contains(ord))
                        throw new Exception("Nie znaleziono pola: " + ord);
                }
                this.methods = order.stream()
                        .map(t-> "get" + t.substring(0, 1).toUpperCase() + t.substring(1))
                        .collect(Collectors.toList());
            }else {
                throw new Exception("Ilość elementów w adnotacji nie zgadza się z ilością pól!");
            }
        }

        this.columnNames = this.methods.stream()
                .map(t-> t.substring(3))
                .collect(Collectors.toList());
    }

    public int getRowCount() {
        return rows.size();
    }

    public int getColumnCount() {
        return rows.get(0).getClass().getDeclaredFields().length;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        String getterName = methods.get(columnIndex);
        Class<?> c = rows.get(rowIndex).getClass();

        try {
            Method m = c.getDeclaredMethod(getterName);
            return m.invoke(rows.get(rowIndex));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getColumnName(int column) {
        return columnNames.get(column);
    }

    public Object getRowAt(int row) {
        return rows.get(row);
    }

    public Class getColumnClass(int column) {
        Class returnValue;
        if ((column >= 0) && (column < getColumnCount())) {
            returnValue = getValueAt(0, column).getClass();
        } else {
            returnValue = Object.class;
        }
        return returnValue;
    }

    public void removeRow(int row){
        rows.remove(row);
    }

    public void addRows(T row){
        //rows = Stream.concat(rows.stream(), row.stream()).collect(Collectors.toList());
        rows.add(row);
    }

    public void updateRow(int row, T data){
        rows.set(row, data);
    }

}
