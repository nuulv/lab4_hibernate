package lab4_hibernate.gui;


import lab4_hibernate.db_model.SlownikEntity;

import javax.swing.*;
import javax.swing.event.ListDataListener;
import java.util.List;

public class CustomComboBoxModel implements ComboBoxModel {

    private List<SlownikEntity> slownikEntityList;

    private SlownikEntity selection = null;

    public CustomComboBoxModel(List<SlownikEntity> entityList){
        slownikEntityList = entityList;
    }


    @Override
    public void setSelectedItem(Object anItem) {
        selection = (SlownikEntity) anItem;
    }

    @Override
    public Object getSelectedItem() {
        return selection;
    }

    @Override
    public int getSize() {
        return slownikEntityList.size();
    }

    @Override
    public Object getElementAt(int index) {
        return slownikEntityList.get(index);
    }

    @Override
    public void addListDataListener(ListDataListener l) {

    }

    @Override
    public void removeListDataListener(ListDataListener l) {

    }
}
