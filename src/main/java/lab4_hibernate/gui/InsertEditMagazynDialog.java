package lab4_hibernate.gui;

import lab4_hibernate.db_model.CardNumber;
import lab4_hibernate.db_model.MagazynpEntity;
import lab4_hibernate.db_model.SlownikEntity;
import lab4_hibernate.util.HibernateUtil;
import org.apache.commons.lang3.math.NumberUtils;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class InsertEditMagazynDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField nr_magField;
    private JTextField nr_kartyField;
    private JTextField nr_klientaField;
    private JTextField firmaField;
    private JTextField jednField;
    private JTextField masaField;
    private JTextField dataField;
    private JComboBox nr_odpaduCombo;
    private JButton editSlownikButton;
    private JButton dodajButton;

    private Session session = MainWindow.session;

    private MagazynpEntity magazynpEntity = null;

    public InsertEditMagazynDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        editSlownikButton.addActionListener(event -> { editSlownikAction(); });
        dodajButton.addActionListener(event -> { dodajButtonAction(); });

        try{
            session.beginTransaction();
            Query query = session.createQuery("from SlownikEntity");
            List<SlownikEntity> slownikEntityList = query.list();

            nr_odpaduCombo.setModel(new CustomComboBoxModel(slownikEntityList));
            nr_odpaduCombo.setRenderer(new DefaultListCellRenderer() {
                @Override
                public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                    super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                    if(value instanceof SlownikEntity){
                        SlownikEntity entity = (SlownikEntity) value;
                        if (entity.getOpis().length() > 50)
                            setText(entity.getNrOdpadu() + ": " + entity.getOpis().substring(0, 50) + "...");
                        else
                            setText(entity.getNrOdpadu() + ": " + entity.getOpis());
                    }
                    return this;
                }
            } );

        }catch (Exception ex){
            ex.printStackTrace();
        }

        magazynpEntity = new MagazynpEntity();
    }

    public InsertEditMagazynDialog(MagazynpEntity entity) {
        this();
        magazynpEntity = entity;

        nr_magField.setEnabled(false);

        nr_magField.setText(magazynpEntity.getNrMag().toString());
        nr_kartyField.setText(magazynpEntity.getNrKarty().toString());
        nr_klientaField.setText(magazynpEntity.getNrKlienta().toString());
        firmaField.setText(magazynpEntity.getFirma().toString());
        jednField.setText(magazynpEntity.getJednostka());
        masaField.setText(magazynpEntity.getMasa().toString());
        dataField.setText(magazynpEntity.getDataD().toString());

        nr_odpaduCombo.getModel().setSelectedItem(magazynpEntity.getSlownikByNrOdpadu());
    }

    private void onOK() {

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date = null;
        try {
            date = format.parse(dataField.getText());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //magazynpEntity.setNrMag(Integer.valueOf(nr_magField.getText()));
        magazynpEntity.setNrKarty(new CardNumber(nr_kartyField.getText()));
        magazynpEntity.setSlownikByNrOdpadu((SlownikEntity) nr_odpaduCombo.getSelectedItem());
        magazynpEntity.setNrKlienta(Integer.valueOf(nr_klientaField.getText()));
        magazynpEntity.setFirma(Integer.valueOf(firmaField.getText()));
        magazynpEntity.setJednostka(jednField.getText());
        magazynpEntity.setMasa(Double.valueOf(masaField.getText()));
        magazynpEntity.setDataD(new Date(date.getTime()));

        dispose();
    }

    private void onCancel() {
        magazynpEntity = null;
        dispose();
    }

    private void editSlownikAction(){
        InsertEditSlownikDialog dialog = new InsertEditSlownikDialog((SlownikEntity) nr_odpaduCombo.getSelectedItem());
        dialog.pack();
        dialog.setVisible(true);

        if(dialog.getSlownikEntity() != null){
            session.beginTransaction();
            session.update(dialog.getSlownikEntity());
            session.getTransaction().commit();
        }
    }

    private void dodajButtonAction(){
        InsertEditSlownikDialog dialog = new InsertEditSlownikDialog();
        dialog.pack();
        dialog.setVisible(true);

        if(dialog.getSlownikEntity() != null){
            session.beginTransaction();
            session.save(dialog.getSlownikEntity());
            session.getTransaction().commit();
        }
    }

    public MagazynpEntity getMagazynpEntity(){
        return magazynpEntity;
    }

    public static void main(String[] args) {
        InsertEditMagazynDialog dialog = new InsertEditMagazynDialog();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
