package lab4_hibernate.gui;

import lab4_hibernate.db_model.SlownikEntity;

import javax.swing.*;
import java.awt.event.*;

public class InsertEditSlownikDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField nr_odpadu_Field;
    private JTextField grupaField;
    private JTextField podgrField;
    private JTextField rodzField;
    private JComboBox typCombo;
    private JTextArea opisTextArea;

    private SlownikEntity slownikEntity = null;

    public InsertEditSlownikDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    public InsertEditSlownikDialog(SlownikEntity entity){
        this();
        slownikEntity = entity;
        nr_odpadu_Field.setEnabled(false);

        nr_odpadu_Field.setText(slownikEntity.getNrOdpadu().toString());
        grupaField.setText(slownikEntity.getGr().toString());
        podgrField.setText(slownikEntity.getPodGr().toString());
        rodzField.setText(slownikEntity.getRodz().toString());
        typCombo.setSelectedIndex((slownikEntity.getTyp().equals("B"))?0:1);
        opisTextArea.setText(slownikEntity.getOpis());

    }

    private void onOK() {
        // add your code here
        slownikEntity.setGr(Integer.valueOf(grupaField.getText()));
        slownikEntity.setPodGr(Integer.valueOf(podgrField.getText()));
        slownikEntity.setRodz(Integer.valueOf(rodzField.getText()));
        slownikEntity.setTyp((String)typCombo.getSelectedItem());
        slownikEntity.setOpis(opisTextArea.getText());
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        slownikEntity = null;
        dispose();
    }

    public SlownikEntity getSlownikEntity(){
        return slownikEntity;
    }

    public static void main(String[] args) {
        //SlownikEntity a = new SlownikEntity(1,1,1,"B","",1, null);
        InsertEditSlownikDialog dialog = new InsertEditSlownikDialog();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
